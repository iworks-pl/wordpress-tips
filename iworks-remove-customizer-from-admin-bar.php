<?php

add_action( 'admin_init', 'iworks_remove', 11);
add_action( 'template_redirect', 'iworks_remove', 11);

function iworks_remove()
{
    remove_action('admin_bar_menu', 'wp_admin_bar_customize_menu', 40);
}


