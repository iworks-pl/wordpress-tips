<?php

class Iworks_Menu_Anchors {

	public function __construct() {
		add_filter( 'wp_nav_menu_objects', array( $this, 'wp_nav_menu_objects' ), 10, 2 );
	}

	public function wp_nav_menu_objects( $items, $args ) {
		if ( 'menu_default' != $args->theme_location ) {
			return $items;
		}
		/**
		 * get peges ids
		 */
		$ids = array();
		foreach ( $this->pages as $key ) {
			$ids = intval( $this->get_option( 'page_'.$key, 0 ) );
		}
		if ( empty( $ids ) ) {
			return $items;
		}
		/**
		 * modify menu
		 */
		foreach ( $items as $key => $item ) {
			if ( ! in_array( $item->object_id, $ids ) ) {
				continue;
			}
			$item->classes[] = 'menu-item-scroll';
			$target_id = $item->object_id;
			$anchor = sprintf( '#post-%d', $target_id );
			if ( is_home() ) {
				$items[ $key ]->url = $anchor;
			} else {
				$items[ $key ]->url = home_url( $anchor );
			}
		}
		return $items;
	}
}

new Iworks_Menu_Anchors();
